import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class ManualTransact {
    public static void main(String[] args) throws SQLException {
        final ConnectionManager connectionManager =
                ConnectionManagerJdbcImpl.getInstance();

        try (Connection connection = connectionManager.getConnection()) {
            DBUtil.renewDatabase(connection);
            try (Statement statement = connection.createStatement()) {
                connection.setAutoCommit(false);
                for (int i = 0; i < 4; i++) {
                    statement.executeUpdate(
                            "INSERT INTO t_art (name, price)\n"
                                    + "VALUES\n"
                                    + "   ('Ring" + i + "', " + i*1000 + ");"
                    );
                }
                Savepoint savepoint = connection.setSavepoint();
                for (int i = 4; i < 8; i++) {
                    statement.executeUpdate(
                            "INSERT INTO t_art (name, price)\n"
                                    + "VALUES\n"
                                    + "   ('Ring" + i + "', " + i*1000 + ");"
                    );
                }
                connection.rollback(savepoint);
                connection.commit();

            } catch (SQLException e) {
                connection.rollback();
                e.printStackTrace();
            }
        }
    }
}