public class Art {
    private Integer art_id;
    private String name;
    private double price;

    public Art() {
    }

    public Art(Integer art_id, String name, double price) {
        this.art_id = art_id;
        this.name = name;
        this.price = price;
    }

    public Integer getId() {
        return art_id;
    }

    public void setId(Integer id) {
        this.art_id = art_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Mobile{" +
                "art_id=" + art_id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}