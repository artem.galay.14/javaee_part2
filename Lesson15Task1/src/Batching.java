import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Batching {
    public static void main(String[] args) throws SQLException {
        Long[] localArgs = new Long[]{1L, 2L, 3L, 4L};
        ArtDao artDao = new ArtDaoImpl();
        artDao.updateArtByArray(localArgs);
        Art art = artDao.getArtById(1L);
        System.out.println(art);
    }
}