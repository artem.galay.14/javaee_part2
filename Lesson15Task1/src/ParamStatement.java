import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ParamStatement {
    public static void main(String[] args) throws SQLException {
        ArtDao artDao = new ArtDaoImpl();
        Art art = artDao.getArtById(1L);
        System.out.println(art);
    }
}