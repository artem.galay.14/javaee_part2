import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {

    private DBUtil() {
    }

    public static void renewDatabase(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement();
        ) {
            statement.execute("-- Database: postgres\n"
                    + "DROP TABLE IF EXISTS t_art;"
                    + "\n"
                    + "CREATE TABLE t_art (\n"
                    + "    art_id bigserial primary key,\n"
                    + "    name varchar(100) NOT NULL,\n"
                    + "    price real NOT NULL);"
                    + "\n"
                    + "INSERT INTO t_art (name, price)\n"
                    + "VALUES\n"
                    + "   ('Pants', 5000),\n"
                    + "   ('Shirt', 2000),\n"
                    + "   ('Hat', 2500),\n"
                    + "   ('Dress', 7000),\n"
                    + "   ('Shoes', 4000);"
                    + "\n"
                    + "DROP TABLE IF EXISTS t_catalog_art;"
                    + "\n"
                    + "CREATE TABLE t_catalog_art (\n"
                    + "    catalog_id int,\n"
                    + "    art_id int);"
                    + "\n"
                    + "INSERT INTO t_catalog_art (catalog_id, art_id)\n"
                    + "VALUES\n"
                    + "   (1, 1),\n"
                    + "   (1, 2),\n"
                    + "   (1, 3),\n"
                    + "   (1, 4),\n"
                    + "   (1, 5);"
                    + "\n"
                    + "DROP TABLE IF EXISTS t_catalogs;"
                    + "\n"
                    + "CREATE TABLE t_catalogs (\n"
                    + "    catalog_id bigserial primary key,\n"
                    + "    name varchar(100) NOT NULL);"
                    + "\n"
                    + "INSERT INTO t_catalogs (name)\n"
                    + "VALUES\n"
                    + "   ('Closes');"
                    + "\n"
                    + "DROP TABLE IF EXISTS t_clients;"
                    + "\n"
                    + "CREATE TABLE t_clients (\n"
                    + "    client_id bigserial primary key,\n"
                    + "    name varchar(100) NOT NULL,\n"
                    + "    phone_number varchar(100) NOT NULL,\n"
                    + "    email varchar(100) NOT NULL);"
                    + "\n"
                    + "INSERT INTO t_clients (name, phone_number, email)\n"
                    + "VALUES\n"
                    + "   ('Bill', '434234', 'bill@mail.ru'),\n"
                    + "   ('Jimm', '123124', 'jimm@mail.ru'),\n"
                    + "   ('Jack', '34534534', 'jack@mail.ru');"
                    + "\n"
            );
        }
    }
}
