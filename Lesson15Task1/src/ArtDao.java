public interface ArtDao {
    Long addArt(Art art, boolean autoCommit);

    Art getArtById(Long art_id);

    boolean updateArtById(Long art_id);
    boolean updateArtByArray(Long[] artArray);

    boolean deleteArtById(Long art_id);
}
